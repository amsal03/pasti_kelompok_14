<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class   HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    //index controller
    public function index()
    {
        $client = new Client();
        $request = $client->get('localhost:8000/api/index');
        $response = $request->getBody();
        $forums = json_decode($response, true);
        return view('home', ['forums' => $forums]);
    }
    //view
    public function view($id) {
        $client = new Client();

        $request = $client->get('localhost:8000/api/view/' . $id);
        $response = $request->getBody();
        $forum = json_decode($response);

        $request = $client->get('localhost:8080/api/index/' . $id);
        $response = $request->getBody();
        $comments = json_decode($response);

        return view('viewForum', ['forum' => $forum, 'comments' => $comments]);
    }
    //create
    public function addForum() {
        return view('addForum');
    }

    public function addForumProcess(Request $request) {
        $client = new Client();
        $data['judul'] = $request->judul;
        $data['isi'] = $request->isi;
        $data['userId'] = Auth::user()->id;
        $client->post('localhost:8000/api/add/',
            ['form_params' => $data]);
        return redirect('/home');
    }
    //update
    public function updateForum($id) {
        $client = new Client();

        $request = $client->get('localhost:8000/api/view/' . $id);
        $response = $request->getBody();
        $forum = json_decode($response);

        return view('updateForum', ['forum' => $forum]);
    }

    public function updateForumProcess(Request $request, $id) {
        $client = new Client();
        $data['judul'] = $request->judul;
        $data['isi'] = $request->isi;
        $client->put('localhost:8000/api/update/' . $id,
            ['form_params' => $data]);
        return redirect('/home');
    }
    //delete
    public function deleteForum($id) {
        $client = new Client();
        $client->delete('localhost:8000/api/delete/' . $id);

        return redirect()->back();
    }

    public function addComment(Request $request, $idForum) {
        $client = new Client();
        $data['isi'] = $request->isi;
        $data['idForum'] = $idForum;
        $data['idUser'] = Auth::user()->id;
        $client->post('localhost:8080/api/create/',
            ['form_params' => $data]);
        return redirect()->back();
    }

    public function updateComment($id, $idForum) {
        $client = new Client();
        $request = $client->get('localhost:8080/api/findComment/' . $id);
        $response = $request->getBody();
        $comment = json_decode($response, true);

        return view('updateComment', ['comment' => $comment, 'forum' => $idForum]);
    }

    public function updateCommentProcess(Request $request, $id, $idForum) {
        $client = new Client();
        $data['isi'] = $request->isi;
        $client->put('localhost:8080/api/update/' . $id,
            ['form_params' => $data]);
        return redirect('/viewForum/' . $idForum);
    }

    public function deleteComment($id) {
        $client = new Client();
        $client->delete('localhost:8080/api/delete/' . $id);

        return redirect()->back();
    }

}
