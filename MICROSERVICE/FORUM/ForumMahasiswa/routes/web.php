<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/viewForum/{id}', 'HomeController@view');

Route::get('/addForum', 'HomeController@addForum');

Route::post('/addForumProcess', 'HomeController@addForumProcess');

Route::get('/updateForum/{id}', 'HomeController@updateForum');

Route::post('/updateForumProcess/{id}', 'HomeController@updateForumProcess');

Route::get('/deleteForum/{id}', 'HomeController@deleteForum');

Route::post('/addComment/{forum}', 'HomeController@addComment');

Route::get('/updateComment/{id}/{forum}', 'HomeController@updateComment');

Route::put('/updateCommentProcess/{id}/{forum}', 'HomeController@updateCommentProcess');

Route::get('/deleteComment/{id}', 'HomeController@deleteComment');

//code diatas merupakan code untuk route dari front end aplikasi forum mahasiswa.

