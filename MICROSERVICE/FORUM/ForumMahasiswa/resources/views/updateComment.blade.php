@extends('layouts.app')

@section('title', 'Update Post')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card bg-light">
                    <div class="card-header">Update Your Post</div>

                    <div class="card-body">
                        <form action="/updateCommentProcess/{{$comment['id']}}/{{$idForum}}" method="post">
                            {{method_field('PUT')}}
                            {{csrf_field()}}
                            <div class="form-group">
                                <textarea class="form-control" name="isi" required>{{$comment['isi']}}</textarea>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Update It!">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
