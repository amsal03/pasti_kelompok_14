@extends('layouts.app')

@section('title', 'Update Post')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div>
                    <h3>Tambah Forum</h3>

                    <div class="card-body">
                        <form action="/addForumProcess" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input class="form-control" name="judul" placeholder="Judul Forum" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="isi" placeholder="Isi Forum" required rows="8"></textarea>
                            </div>
                            <input class="btn btn-success" type="submit" value="Tambah">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
