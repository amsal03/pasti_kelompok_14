@extends('layouts.app')

@section('title', 'Threads')

//Halaman Utama untuk menampilakan forum

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <a class="btn btn-success" href="/addForum">Tambah Forum</a>
                <br><br>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width:300px;">Author</th>
                            <th>Forum</th>
                            <th style="width:300px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($forums as $forum)
                            <?php
                            $client = new \GuzzleHttp\Client();

                            $request = $client->get('localhost:8240/api/getUser/' . $forum['userId']);
                            $response = $request->getBody();
                            $user = json_decode($response, true);

                            $request = $client->get('localhost:8080/api/getComment/' . $forum['id']);
                            $response = $request->getBody();
                            $comment = json_decode($response, true);
                            ?>

                            <tr class="header">
                                <td>
                                    {{$user['name']}}
                                </td>
                                <td>
                                    <a class="h5"><b>{{$forum['judul']}}</b></a>
                                </td>
                                <td>
                                    <div class="">
                                    <a class="btn btn-success"
                                       href="/viewForum/{{$forum['id']}}">Comment</a>
                                    @if($forum['userId'] == Auth::user()->id)

                                            <a class="btn btn-primary"
                                               href="/updateForum/{{$forum['id']}}">Update</a>
                                            <a class="btn btn-danger"
                                               href="/deleteForum/{{$forum['id']}}">Delete</a>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
