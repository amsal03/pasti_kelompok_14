@extends('layouts.app')

@section('title', 'View Thread')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <?php
                $client = new \GuzzleHttp\Client();

                $request = $client->get('localhost:8240/api/getUser/' . $forum->userId);
                $response = $request->getBody();
                $user = json_decode($response, true);

                $request = $client->get('localhost:8080/api/getComment/' . $forum->id);
                $response = $request->getBody();
                $comment = json_decode($response, true);
                ?>

                <div class="card">

                    <div class="card-body">
                        <h5><b>{{$forum->judul}}</b></h5>
                        <hr class="bg-dark">
                        <p>{{$forum->isi}}</p>
                        <br>
                        @foreach($comments as $comment)
                            <?php
                            $request = $client->get('localhost:8240/api/getUser/' . $comment->idUser);
                            $response = $request->getBody();
                            $user = json_decode($response, true);
                            ?>

                            <table class="table">
                                <tr>
                                    <td style="width: 250px;"> {{$user['name']}}</td>
                                    <td style="width:600px;">{{$comment->isi}}</td>
{{--                                    <td colspan="2">--}}
{{--                                        @if($forum->userId == Auth::user()->id)--}}
{{--                                            <a class="btn btn-primary"--}}
{{--                                               href="/updateComment/{{$comment->id}}/{{$forum->id}}">Update</a>--}}
{{--                                            <a class="btn btn-danger" href="/deleteComment/{{$comment->id}}">Delete</a>--}}
{{--                                        @endif--}}
{{--                                    </td>--}}
                                </tr>
                            </table>
                            <br>
                        @endforeach


                        <div class="card-body">
                            <form action="/addComment/{{$forum->id}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <textarea class="form-control" name="isi" placeholder="Comment"
                                              required></textarea>
                                </div>
                                <input class="btn btn-success" type="submit" value="Comment">
                            </form>

                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
@endsection
