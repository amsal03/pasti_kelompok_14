<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//microservice comment

Route::get('/index/{idForum}', 'CommentController@index');

Route::get('/getComment/{idForum}', 'CommentController@getComment');

Route::get('/findComment/{id}', 'CommentController@findComment');

Route::post('/create', 'CommentController@create');

Route::put('/update/{id}', 'CommentController@update');

Route::delete('/delete/{id}', 'CommentController@delete');
