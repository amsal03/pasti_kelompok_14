<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index($idForum)
    {
        $comments = Comment::where('idForum', $idForum)->get();

        return response()->json($comments);
    }

    public function getComment($idForum)
    {
        $comment = Comment::where('idForum', $idForum)->get()->last();

        return response()->json($comment);
    }

    public function findComment($id)
    {
        $comment = Comment::find($id);

        return response()->json($comment);
    }

    public function create(Request $request)
    {
        $comment = new Comment();

        $comment->isi = $request->isi;
        $comment->idForum = $request->idForum;
        $comment->idUser = $request->idUser;

        $comment->save();
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);

        $comment->isi = $request->isi;

        $comment->save();
    }

    public function delete($id)
    {
        $comment = Comment::find($id);

        $comment->delete();
    }


}
