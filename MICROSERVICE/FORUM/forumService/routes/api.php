<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//microservice forum

Route::get('/index', 'ForumController@index');

Route::get('/view/{id}', 'ForumController@getForum');

Route::post('/add', 'ForumController@addForum');

Route::put('/update/{id}', 'ForumController@updateForum');

Route::delete('/delete/{id}', 'ForumController@deleteForum');
