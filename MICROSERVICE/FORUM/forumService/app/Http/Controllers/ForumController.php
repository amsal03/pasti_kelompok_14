<?php

namespace App\Http\Controllers;

use App\Forum;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function index() {
        $forums = Forum::orderBy('id', 'desc')->get();
        return response()->json($forums);
    }

    public function getForum($id) {
        $forums = Forum::find($id);
        return response()->json($forums);
    }

    public function addForum(Request $request) {
        $forum = new Forum();

        $forum->judul = $request->judul;
        $forum->isi = $request->isi;
        $forum->userId = $request->userId;

        $forum->save();
    }

    public function updateForum(Request $request, $id) {
        $forum = Forum::find($id);

        $forum->judul = $request->judul;
        $forum->isi = $request->isi;

        $forum->save();
    }

    public function deleteForum($id) {
        $forum = Forum::find($id);

        $forum->delete();
    }
}
