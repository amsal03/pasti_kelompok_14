<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/forum', 'ForumController@index');

Route::get('/forum/tambah','ForumController@tambah');

Route::post('/forum/proses','ForumController@proses');

Route::get('/forum/edit/{id}','ForumController@edit');

Route::get('/forum/update','ForumController@update');