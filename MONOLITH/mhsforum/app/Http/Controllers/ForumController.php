<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ForumController extends Controller
{
	public function index(){

    // mengambil data dari table forum
    	$forum = DB::table('forum')->get();
 
    	// mengirim data forum ke view index
    	return view('index',['forum' => $forum]);
    }

    public function tambah()
	{
	 
		// memanggil view tambah
		return view('tambah');
	 
	}

	public function proses(Request $request)
	{
		// insert data ke table forum
		DB::table('forum')->insert([
			'nama' => $request->nama,
			'fakultas' => $request->fakultas,
			'umur' => $request->umur,
			'deskripsi' => $request->deskripsi
		]);
		// alihkan halaman ke halaman forum
		return redirect('/forum');
	 
	}

	public function edit($id)
	{
		// mengambil data forum berdasarkan id yang dipilih
		$forum = DB::table('forum')->where('Id',$id)->get();
		// passing data forum yang didapat ke view edit.blade.php
		return view('edit',['forum' => $forum]);
	}

	public function update(Request $request)
{
	// update data forum
	DB::table('forum')->where('id',$request->id)->update([
		'nama' => $request->nama,
		'fakultas' => $request->fakultas,
		'umur' => $request->umur,
		'deskripsi' => $request->deskripsi
	]);
	// alihkan halaman ke halaman forum
	return redirect('/forum');
}
}
