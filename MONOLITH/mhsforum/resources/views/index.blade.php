<!DOCTYPE html>
<html>
<head>
	<title>Forum Mahasiswa</title>
</head>
<body>
 
	<h2>www.forumMahasiswa.com</h2>
	<h3>Data Mahasiwa</h3>
 
	<a href="/forum/tambah"> + Tambah Mahasiswa</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>Nama</th>
			<th>Fakultas</th>
			<th>Umur</th>
			<th>Deskripsi</th>
			<th>Opsi</th>
		</tr>
		@foreach($forum as $p)
		<tr>
			<td>{{ $p->nama }}</td>
			<td>{{ $p->fakultas }}</td>
			<td>{{ $p->umur }}</td>
			<td>{{ $p->deskripsi }}</td>
			<td>
				<a href="/forum/edit/{{ $p->id }}">Edit</a>
				|
				<a href="/forum/hapus/{{ $p->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
 
 
</body>
</html>