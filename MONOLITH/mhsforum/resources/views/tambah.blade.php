<!DOCTYPE html>
<html>
<head>
	<title>Forum Mahasiswa</title>
</head>
<body>
 
	<h2><a href="https://www.forumMahasiswa.com">www.forumMahasiswa.com</a></h2>
	<h3>Data Mahasiswa</h3>
 
	<a href="/forum"> Kembali</a>
	
	<br/>
	<br/>
 
	<form action="/forum/proses" method="post">
		{{ csrf_field() }}
		Nama <input type="text" name="nama" required="required"> <br/>
		Fakultas <input type="text" name="fakultas" required="required"> <br/>
		Umur <input type="number" name="umur" required="required"> <br/>
		Deskripsi <textarea name="deskripsi" required="required"></textarea> <br/>
		<input type="submit" value="Simpan Data">
	</form>
 
</body>
</html>