<!DOCTYPE html>
<html>
<head>
	<title>Forum Mahasiswa</title>
</head>
<body>
 
	<h2><a href="https://www.forumMahasiswa.com">www.forumMahasiswa.com</a></h2>
	<h3>Edit Forum</h3>
 
	<a href="/pegawai"> Kembali</a>
	
	<br/>
	<br/>
 
	@foreach($forum as $p)
	<form action="/forum/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $p->id }}"> <br/>

		Nama <input type="text" required="required" name="nama" value="{{ $p->nama }}"> <br/>

		Fakultas <input type="text" required="required" name="fakultas" value="{{ $p->fakultas }}"> <br/>

		Umur <input type="number" required="required" name="umur" value="{{ $p->umur }}"> <br/>

		Deskripsi <textarea required="required" name="deskripsi">{{ $p->deskripsi }}</textarea> <br/>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
		
 
</body>
</html>